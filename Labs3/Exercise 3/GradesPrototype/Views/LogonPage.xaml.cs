﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Views
{
    /// <summary>
    /// Interaction logic for LogonPage.xaml
    /// </summary>
    public partial class LogonPage : UserControl
    {
        public LogonPage()
        {
            InitializeComponent();
        }

        #region Event Members
        public event EventHandler LogonSuccess;

        // Exercise 3: Task 1a: Define LogonFailed event
        public delegate void MyEventHandler(object sender, MyEventArgs e);
        public event MyEventHandler LogonFailed;

        #endregion

        #region Logon Validation

        // Exercise 3: Task 1b: Validate the username and password against the Users collection in the MainWindow window
        private void Logon_Click(object sender, RoutedEventArgs e)
        {
            ArrayList teachers = DataSource.Teachers;
            ArrayList students = DataSource.Students;

            var teacherArr = (from Teacher t in teachers where t.UserName == username.Text select t).ToArray();
            var studentArr = (from Student s in students where s.UserName == username.Text select s).ToArray();

            if(teacherArr.Length == 0 && studentArr.Length == 0)
            {
                LogonFailed(this,new MyEventArgs("Logon Failed \n UserName not exist"));
                return;
            }
            else if(teacherArr.Length > 0)
            {
                if(teacherArr[0].Password != password.Password)
                {
                    LogonFailed(this, new MyEventArgs("Logon Failed \n password wrong"));
                    return;
                }
                SessionContext.CurrentTeacher = teacherArr[0];
                SessionContext.UserID = teacherArr[0].TeacherID;
                SessionContext.UserName = teacherArr[0].UserName;
                SessionContext.UserRole = Role.Teacher;
            } 
            else
            {
                if (studentArr[0].Password != password.Password)
                {
                    LogonFailed(this, new MyEventArgs("Logon Failed \n password wrong"));
                    return;
                }
                SessionContext.CurrentStudent = studentArr[0];
                SessionContext.UserID = studentArr[0].TeacherID;
                SessionContext.UserName = studentArr[0].UserName;
                SessionContext.UserRole = Role.Student;
            }

            LogonSuccess(this, null);
        }
        #endregion
    }
    public class MyEventArgs : EventArgs
    {
        public MyEventArgs(string data)
        {
            this.data = data;
        }
        
        private string data;

        public string Data
        {
            get { return data; }
        }
    }
}
