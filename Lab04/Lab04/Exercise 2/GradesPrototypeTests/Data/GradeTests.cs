﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradesPrototype.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradesPrototype.Data.Tests
{
    [TestClass()]
    public class GradeTests
    {
        [TestInitialize]
        public void Init()
        {
            DataSource.CreateData();
        }

        [TestMethod()]
        public void TestValidGrade()
        {
            Grade grade = new Grade(15,DateTime.Now.ToString("d"), "Math", "A+","hello");
            Assert.AreEqual(grade.StudentID, 15);
            Assert.AreEqual(grade.AssessmentDate, DateTime.Now.ToString("d"));
            Assert.AreEqual(grade.Assessment, "A+");
            Assert.AreEqual(grade.SubjectName, "Math");
            Assert.AreEqual(grade.Comments, "hello");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestBadDate()
        {
            Grade grade = new Grade(15, "10/10/2025", "Math", "A+", "hello");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataTestMethod]
        [DataRow("32/12/2020")]
        [DataRow("30/02/2020")]
        public void TestDateNotRecognized(string date)
        {
            Grade grade = new Grade(15, date, "Math", "A+", "hello");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataTestMethod]
        [DataRow("M")]
        [DataRow("N")]
        public void TestBadAssessment(string assessment)
        {
            Grade grade = new Grade(15, DateTime.Now.ToString("d"), "Math", assessment, "hello");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        [DataTestMethod]
        [DataRow("LY")]
        [DataRow("HOA")]
        public void TestBadSubject(string subject)
        {
            Grade grade = new Grade(15, DateTime.Now.ToString("d"), subject, "A+", "hello");
        }
    }
}