﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GradesPrototype.Data
{
    // Types of user
    public enum Role { Teacher, Student };

    // WPF Databinding requires properties

    // Exercise 1: Task 1a: Convert Grade into a class and define constructors
    public class Grade
    {
        public int StudentID { get; set; }
        public string AssessmentDate { get; set; }
        public string SubjectName { get; set; }
        public string Assessment { get; set; }
        public string Comments { get; set; }

        public Grade()
        {

        }
        public Grade(int studentID, string assessmentDate, string subjectName, string assessment, string comments)
        {
            this.StudentID = studentID;
            this.AssessmentDate = assessmentDate;
            this.SubjectName = subjectName;
            this.Assessment = assessment;
            this.Comments = comments;
        }
    }

    // Exercise 1: Task 2a: Convert Student into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Student
    {
        public int StudentID { get; set; }
        public string UserName { get; set; }
        private string _password;
        public string Password
        {
            set { _password = value; }
        }
        public int TeacherID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool VerifyPassword(string password)
        {
            return password == _password ? true : false;
        }
    }

    // Exercise 1: Task 2b: Convert Teacher into a class, make the password property write-only, add the VerifyPassword method, and define constructors
    public class Teacher
    {
        public int TeacherID { get; set; }
        public string UserName { get; set; }
        private string _password;
        public string Password {
            set {
                _password = value;
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Class { get; set; }

        public Teacher()
        {

        }

        public Teacher(int teacherId, string userName, string password, string firstName, string lastName, string className)
        {
            this.TeacherID = teacherId;
            this.UserName = userName;
            this._password = password;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Class = className;
        }

        public bool VerifyPassword(string password)
        {
            return password == _password ? true : false;
        }
    }
    
}
