﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GradesPrototype.Data;
using GradesPrototype.Services;

namespace GradesPrototype.Controls
{
    /// <summary>
    /// Interaction logic for AssignStudentDialog.xaml
    /// </summary>
    public partial class AssignStudentDialog : Window
    {
        public AssignStudentDialog()
        {
            InitializeComponent();
        }

        // Exercise 4: Task 3b: Refresh the display of unassigned students
        private void Refresh()
        {
            txtMessage.Visibility = Visibility.Collapsed;
            list.Visibility = Visibility.Visible;

            List<Student> students = DataSource.Students.FindAll(x => x.TeacherID == 0);
            list.ItemsSource = students;
        }

        private void AssignStudentDialog_Loaded(object sender, RoutedEventArgs e)
        {
            Refresh();
        }

        delegate void EventHandlerStudentClick(object sender, EventHandler e);
        //  Exercise 4: Task 3a: Enroll a student in the teacher's class
        private void Student_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Student student = sender;
                Button itemClicked = sender as Button;
                if (itemClicked != null)
                {
                    // Find out which student was clicked
                    Student student = (Student)itemClicked.DataContext;
                    string message = String.Format("Add {0} {1} to your class?", student.FirstName, student.LastName);
                    MessageBoxResult reply = MessageBox.Show(message, "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (reply == MessageBoxResult.Yes)
                    {
                        SessionContext.CurrentTeacher.EnrollStudent(student);
                        Refresh();
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error enrolling student", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            

        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            // Close the dialog box
            this.Close();
        }
    }
}
